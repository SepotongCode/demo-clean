package com.demo.clean.base.command;

import com.demo.clean.base.dto.request.ServiceRequest;

public interface Command<REQUEST extends ServiceRequest, RESPONSE>{

    RESPONSE execute(REQUEST request);

}

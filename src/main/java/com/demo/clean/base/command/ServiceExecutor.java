package com.demo.clean.base.command;

import com.demo.clean.base.dto.request.ServiceRequest;

public interface ServiceExecutor {

    <REQUEST extends ServiceRequest, RESPONSE> RESPONSE execute(Class<? extends Command<REQUEST,RESPONSE>> commandClass, REQUEST request);

}

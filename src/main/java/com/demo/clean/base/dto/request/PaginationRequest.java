package com.demo.clean.base.dto.request;

import com.demo.clean.base.enums.SortBy;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Data
public class PaginationRequest implements ServiceRequest {

    protected int page;

    protected int size;

    protected String orderBy;

    @Enumerated(EnumType.STRING)
    protected SortBy sortBy;
}

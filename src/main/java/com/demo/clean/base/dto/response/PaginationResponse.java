package com.demo.clean.base.dto.response;

import com.demo.clean.base.dto.request.PaginationRequest;

public class PaginationResponse extends PaginationRequest {

    private long totalElements;

    private int totalPages;

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}

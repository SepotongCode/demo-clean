package com.demo.clean.base.exceptions;

import com.demo.clean.base.dto.request.BaseExceptionRequest;

public interface AbstractException {

    <T> T buildResponse(BaseExceptionRequest exception);

}

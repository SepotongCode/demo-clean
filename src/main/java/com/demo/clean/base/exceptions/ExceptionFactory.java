package com.demo.clean.base.exceptions;

import com.demo.clean.base.dto.request.BaseExceptionRequest;

public interface ExceptionFactory {

    AbstractException buildException(BaseExceptionRequest exception);

}

package com.demo.clean.base.exceptions.catcher;

import com.demo.clean.base.dto.request.BaseExceptionRequest;
import com.demo.clean.base.controller.dto.response.BaseResponse;
import com.demo.clean.base.exceptions.AbstractException;
import com.demo.clean.base.exceptions.thrower.DataExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DataExistsExceptionHttp implements AbstractException {

    @Override
    public ResponseEntity buildResponse(BaseExceptionRequest exception) {

        if (exception.getException() instanceof DataExistsException data) {

            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(
                            BaseResponse.builder()
                                    .errors(data.getErrors())
                                    .build()
                    );
        }

        throw new RuntimeException("not handle exception",exception.getException());
    }

}

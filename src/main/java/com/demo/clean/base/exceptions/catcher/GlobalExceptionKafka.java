package com.demo.clean.base.exceptions.catcher;

import com.demo.clean.base.dto.request.BaseExceptionRequest;
import com.demo.clean.base.exceptions.AbstractException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class GlobalExceptionKafka implements AbstractException {

    @Override
    public String buildResponse(BaseExceptionRequest exception) {


        return "Error Kafka global";
    }

}

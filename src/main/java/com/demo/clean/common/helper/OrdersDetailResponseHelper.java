package com.demo.clean.common.helper;

import com.demo.clean.dto.response.OrdersItemResponse;
import com.demo.clean.dto.response.OrdersDetailResponse;
import com.demo.clean.entity.Orders;
import com.demo.clean.entity.OrdersItem;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;

@NoArgsConstructor
public class OrdersDetailResponseHelper {

    public static OrdersDetailResponse buildResponse(Orders order, List<OrdersItem> ordersItems) {
        OrdersDetailResponse ordersResponse = new OrdersDetailResponse();
        BeanUtils.copyProperties(order,ordersResponse);
        ordersResponse.setId(order.getId());
        ordersResponse.setEntity(order);

        List<OrdersItemResponse> ordersItemResponses = ordersItems.stream()
                .map(ordersItem -> {
                    OrdersItemResponse ordersItemResponse = new OrdersItemResponse();
                    BeanUtils.copyProperties(ordersItem,ordersItemResponse);
                    ordersItemResponse.setId(ordersItem.getId());
                    ordersItemResponse.setOrdersId(ordersItem.getOrders().getId());
                    return ordersItemResponse;
                }).toList();

        ordersResponse.setOrdersItems(ordersItemResponses);

        return ordersResponse;

    }

    public static OrdersDetailResponse buildOrderResponse(Orders order, List<OrdersItem> ordersItems) {
        OrdersDetailResponse ordersResponse = new OrdersDetailResponse();
        BeanUtils.copyProperties(order,ordersResponse);
        ordersResponse.setId(order.getId());
        ordersResponse.setEntity(order);

        List<OrdersItemResponse> ordersItemResponses = ordersItems.stream()
                .map(ordersItem -> {
                    OrdersItemResponse ordersItemResponse = new OrdersItemResponse();
                    BeanUtils.copyProperties(ordersItem,ordersItemResponse);
                    ordersItemResponse.setId(ordersItem.getId());
                    ordersItemResponse.setOrdersId(ordersItem.getOrders().getId());
                    return ordersItemResponse;
                }).toList();

        ordersResponse.setOrdersItems(ordersItemResponses);

        return ordersResponse;

    }
}

package com.demo.clean.controller.api;

import com.demo.clean.base.command.ServiceExecutor;
import com.demo.clean.base.controller.dto.response.BaseResponse;
import com.demo.clean.dto.request.OrdersCreateRequest;
import com.demo.clean.dto.response.OrdersDetailResponse;
import com.demo.clean.dto.response.OrdersResponse;
import com.demo.clean.service.contract.OrdersCreateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class OrdersCreateController {

    private final ServiceExecutor serviceExecutor;

    @PostMapping("/orders/create")
    public ResponseEntity doExecute(@RequestBody @Valid OrdersCreateRequest request) {
        OrdersDetailResponse response =  serviceExecutor.execute(OrdersCreateService.class,request);

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(BaseResponse.builder()
                        .data(response)
                        .build());

    }

}

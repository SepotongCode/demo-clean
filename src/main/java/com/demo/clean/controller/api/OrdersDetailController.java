package com.demo.clean.controller.api;

import com.demo.clean.base.command.ServiceExecutor;
import com.demo.clean.base.controller.dto.response.BaseResponse;
import com.demo.clean.base.dto.request.UniqueRequest;
import com.demo.clean.dto.request.OrdersCreateRequest;
import com.demo.clean.dto.response.OrdersDetailResponse;
import com.demo.clean.dto.response.OrdersResponse;
import com.demo.clean.service.contract.OrdersCreateService;
import com.demo.clean.service.contract.OrdersDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class OrdersDetailController {

    private final ServiceExecutor serviceExecutor;

    @GetMapping("/orders/detail/{id}")
    public ResponseEntity doExecute(@PathVariable("id") Long id) {
        OrdersDetailResponse response =  serviceExecutor.execute(OrdersDetailService.class,
                UniqueRequest.<Long>builder()
                        .value(id)
                        .build());
        
        return ResponseEntity.status(HttpStatus.OK)
                .body(BaseResponse.builder()
                        .data(response)
                        .build());

    }

}

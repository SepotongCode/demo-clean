package com.demo.clean.dto.criteria;

import com.demo.clean.base.dto.request.PaginationRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrdersItemCriteria extends PaginationRequest {

    private String ordersInvoice;

    private String name;

}

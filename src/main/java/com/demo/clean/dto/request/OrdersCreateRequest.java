package com.demo.clean.dto.request;

import com.demo.clean.base.dto.request.ServiceRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrdersCreateRequest implements ServiceRequest {

    @NotNull
    @NotEmpty
    @NotBlank
    private String invoice;

    @NotNull
    private BigDecimal amount;

    private List<OrdersItemCreateRequest> ordersItems = new ArrayList<>();

    ///nambah

}

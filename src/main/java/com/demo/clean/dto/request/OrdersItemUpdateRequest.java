package com.demo.clean.dto.request;

import com.demo.clean.base.dto.request.ServiceRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrdersItemUpdateRequest implements ServiceRequest {

    @NotNull
    private Integer id;

    @NotNull
    private Integer ordersId;

    @NotNull
    @NotBlank
    @NotEmpty
    private String name;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private Integer qty;

}

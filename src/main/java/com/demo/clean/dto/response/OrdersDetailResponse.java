package com.demo.clean.dto.response;

import com.demo.clean.entity.Orders;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrdersDetailResponse {

    private Long id;

    private String invoice;

    private BigDecimal amount;

    private List<OrdersItemResponse> ordersItems = new ArrayList<>();

    @JsonIgnore
    private Orders entity;

}

package com.demo.clean.dto.response;

import com.demo.clean.entity.OrdersItem;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrdersItemResponse {

    private Long id;

    private Long ordersId;

    private String name;

    private BigDecimal amount;

    private Integer qty;

    @JsonIgnore
    private OrdersItem entity;

}

package com.demo.clean.entity;

import com.demo.clean.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "orders")
public class Orders extends BaseIdEntity {

    @Column(name = "invoice")
    private String invoice;

    @Column(name = "amount")
    private BigDecimal amount;

}

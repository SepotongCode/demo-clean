package com.demo.clean.entity;

import com.demo.clean.base.entity.BaseIdEntity;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "orders_items")
public class OrdersItem extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "orders_id")
    private Orders orders;

    @Column(name = "name")
    private String name;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "qty")
    private Integer qty;

}

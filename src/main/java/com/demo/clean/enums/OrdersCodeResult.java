package com.demo.clean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OrdersCodeResult {

    NOT_FOUND("404","data not found"),
    EXISTS("400","data exists");

    private String httpStatusCode;

    private String message;

}

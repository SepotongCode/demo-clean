package com.demo.clean.exceptions.catcher;

import com.demo.clean.base.controller.dto.response.BaseResponse;
import com.demo.clean.base.dto.request.BaseExceptionRequest;
import com.demo.clean.base.exceptions.AbstractException;
import com.demo.clean.exceptions.thrower.OrdersExistException;
import com.demo.clean.exceptions.thrower.OrdersNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrdersExistExceptionHttp implements AbstractException {

    @Override
    public ResponseEntity buildResponse(BaseExceptionRequest exception) {

        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

        if (exception.getException() instanceof OrdersExistException data) {

            log.error("handle error in requestTypeHttp: {}", data.getErrors());

            return ResponseEntity
                            .status(httpStatus)
                            .body(
                                    BaseResponse.builder()
                                            .errors(data.getErrors())
                                            .build()
                            );
        }

        throw new RuntimeException("not handle exception",exception.getException());
    }
}

package com.demo.clean.exceptions.catcher;

import com.demo.clean.base.dto.request.BaseExceptionRequest;
import com.demo.clean.base.exceptions.AbstractException;
import com.demo.clean.exceptions.thrower.OrdersExistException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrdersExistExceptionKafka implements AbstractException {

    @Override
    public String buildResponse(BaseExceptionRequest exception) {

        if (exception.getException() instanceof OrdersExistException data) {

            log.error("handle error in requestTypeKafka: {}", data.getErrors());

            return data.getErrors().toString();

        }

        throw new RuntimeException("not handle exception",exception.getException());
    }
}

package com.demo.clean.exceptions.thrower;

import com.demo.clean.base.dto.response.ErrorResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OrdersExistException extends RuntimeException {

    private List<ErrorResponse> errors = new ArrayList<>();

    public OrdersExistException(String message) {
        super(message);
    }

    public OrdersExistException(String message, List<ErrorResponse> errors) {
        super(message);
        this.errors = errors;
    }

    public OrdersExistException(String message, List<ErrorResponse> errors, Throwable throwable) {
        super(message, throwable);
        this.errors = errors;
    }

}

package com.demo.clean.exceptions.thrower;

import com.demo.clean.base.dto.response.ErrorResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OrdersNotFoundException extends RuntimeException{

    private List<ErrorResponse> errors = new ArrayList<>();

    public OrdersNotFoundException(String message) {
        super(message);
    }

    public OrdersNotFoundException(String message, List<ErrorResponse> errors) {
        super(message);
        this.errors = errors;
    }

    public OrdersNotFoundException(String message, List<ErrorResponse> errors, Throwable throwable) {
        super(message, throwable);
        this.errors = errors;
    }

}

package com.demo.clean.repository;

import com.demo.clean.base.repository.BaseJpaRepository;
import com.demo.clean.entity.Orders;
import com.demo.clean.entity.OrdersItem;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersItemRepository extends BaseJpaRepository<OrdersItem,Long>, JpaSpecificationExecutor<OrdersItem> {

    List<OrdersItem> findByOrdersId(Long id);

}

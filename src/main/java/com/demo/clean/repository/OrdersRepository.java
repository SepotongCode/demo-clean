package com.demo.clean.repository;

import com.demo.clean.base.repository.BaseJpaRepository;
import com.demo.clean.entity.Orders;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrdersRepository extends BaseJpaRepository<Orders,Long>, JpaSpecificationExecutor<Orders> {

    Optional<Orders> findByInvoice(String invoice);

    Optional<Orders> findById(Integer id);

}

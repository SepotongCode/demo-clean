package com.demo.clean.service.contract;

import com.demo.clean.base.command.Command;
import com.demo.clean.dto.request.OrdersCreateRequest;
import com.demo.clean.dto.response.OrdersDetailResponse;

public interface OrdersCreateService extends Command<OrdersCreateRequest, OrdersDetailResponse> {
}

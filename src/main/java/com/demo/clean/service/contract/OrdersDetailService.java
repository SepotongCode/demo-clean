package com.demo.clean.service.contract;

import com.demo.clean.base.command.Command;
import com.demo.clean.base.dto.request.UniqueRequest;
import com.demo.clean.dto.request.OrdersCreateRequest;
import com.demo.clean.dto.response.OrdersDetailResponse;
import com.demo.clean.dto.response.OrdersResponse;

public interface OrdersDetailService extends Command<UniqueRequest<Long>, OrdersDetailResponse> {
}

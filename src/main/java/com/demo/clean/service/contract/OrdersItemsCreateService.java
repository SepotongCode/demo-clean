package com.demo.clean.service.contract;

import com.demo.clean.base.command.Command;
import com.demo.clean.dto.request.OrdersCreateRequest;
import com.demo.clean.dto.request.OrdersItemsCreateRequest;
import com.demo.clean.dto.response.OrdersItemResponse;
import com.demo.clean.dto.response.OrdersResponse;

import java.util.List;

public interface OrdersItemsCreateService extends Command<OrdersItemsCreateRequest, List<OrdersItemResponse>> {
}

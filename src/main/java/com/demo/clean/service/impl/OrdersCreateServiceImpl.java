package com.demo.clean.service.impl;

import com.demo.clean.base.command.ServiceExecutor;
import com.demo.clean.dto.request.OrdersCreateRequest;
import com.demo.clean.dto.request.OrdersItemsCreateRequest;
import com.demo.clean.dto.response.OrdersDetailResponse;
import com.demo.clean.dto.response.OrdersItemResponse;
import com.demo.clean.entity.Orders;
import com.demo.clean.repository.OrdersRepository;
import com.demo.clean.service.contract.OrdersCreateService;
import com.demo.clean.service.contract.OrdersItemsCreateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrdersCreateServiceImpl implements OrdersCreateService {

    private final OrdersRepository ordersRepository;

    private final ServiceExecutor serviceExecutor;

    @Transactional
    @Override
    public OrdersDetailResponse execute(OrdersCreateRequest request) {

        Orders order = saveOrder(request);

        List<OrdersItemResponse> ordersItems = serviceExecutor.execute(OrdersItemsCreateService.class, OrdersItemsCreateRequest.builder()
                        .ordersId(order.getId())
                        .ordersItems(request.getOrdersItems())
                .build());

        return buildResponse(order, ordersItems);
    }

    private OrdersDetailResponse buildResponse(Orders order, List<OrdersItemResponse> ordersItems) {
        OrdersDetailResponse ordersResponse = new OrdersDetailResponse();
        BeanUtils.copyProperties(order,ordersResponse);
        ordersResponse.setId(order.getId());
        ordersResponse.setEntity(order);

        ordersResponse.setOrdersItems(ordersItems);

        return ordersResponse;

    }

    private Orders saveOrder(OrdersCreateRequest request) {
        Orders orders = new Orders();
        BeanUtils.copyProperties(request,orders);
        orders.setCreateDate(new Date());
        ordersRepository.save(orders);
        return orders;
    }


}

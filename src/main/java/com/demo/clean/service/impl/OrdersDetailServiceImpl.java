package com.demo.clean.service.impl;

import com.demo.clean.base.command.ServiceExecutor;
import com.demo.clean.base.dto.request.UniqueRequest;
import com.demo.clean.base.dto.response.ErrorResponse;
import com.demo.clean.common.helper.OrdersDetailResponseHelper;
import com.demo.clean.dto.response.OrdersDetailResponse;
import com.demo.clean.entity.Orders;
import com.demo.clean.entity.OrdersItem;
import com.demo.clean.enums.OrdersCodeResult;
import com.demo.clean.exceptions.thrower.OrdersNotFoundException;
import com.demo.clean.repository.OrdersItemRepository;
import com.demo.clean.repository.OrdersRepository;
import com.demo.clean.service.contract.OrdersDetailService;
import com.demo.clean.service.query.contract.OrdersFindByIdService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrdersDetailServiceImpl implements OrdersDetailService {

    private final OrdersRepository ordersRepository;

    private final OrdersItemRepository ordersItemRepository;

    private final ServiceExecutor serviceExecutor;

    @Override
    public OrdersDetailResponse execute(UniqueRequest<Long> request) {

        Orders order = getOrder(request);

        List<OrdersItem> ordersItems = getOrderItems(order);

        return OrdersDetailResponseHelper.buildResponse(order, ordersItems);
    }

    private List<OrdersItem> getOrderItems(Orders order) {
        return ordersItemRepository.findByOrdersId(order.getId());
    }

   /* private Orders getOrder(UniqueRequest<Long> request) {

        return ordersRepository.findById(request.getValue())
                .orElseThrow(() -> new OrdersNotFoundException(OrdersCodeResult.NOT_FOUND.getMessage(),
                        Arrays.asList(
                                ErrorResponse.builder()
                                        .code(OrdersCodeResult.NOT_FOUND.getHttpStatusCode())
                                        .field("id")
                                        .message(OrdersCodeResult.NOT_FOUND.getMessage())
                                        .build()
                        )
                ));
    }*/

    private Orders getOrder(UniqueRequest<Long> request) {

        return serviceExecutor.execute(OrdersFindByIdService.class, request).getEntity();
    }

}

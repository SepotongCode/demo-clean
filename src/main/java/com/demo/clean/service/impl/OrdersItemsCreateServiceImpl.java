package com.demo.clean.service.impl;

import com.demo.clean.base.command.ServiceExecutor;
import com.demo.clean.base.dto.request.UniqueRequest;
import com.demo.clean.base.dto.response.ErrorResponse;
import com.demo.clean.dto.request.OrdersItemCreateRequest;
import com.demo.clean.dto.request.OrdersItemsCreateRequest;
import com.demo.clean.dto.response.OrdersItemResponse;
import com.demo.clean.entity.Orders;
import com.demo.clean.entity.OrdersItem;
import com.demo.clean.enums.OrdersCodeResult;
import com.demo.clean.exceptions.thrower.OrdersExistException;
import com.demo.clean.exceptions.thrower.OrdersNotFoundException;
import com.demo.clean.repository.OrdersItemRepository;
import com.demo.clean.repository.OrdersRepository;
import com.demo.clean.service.contract.OrdersItemsCreateService;
import com.demo.clean.service.query.contract.OrdersFindByIdService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrdersItemsCreateServiceImpl implements OrdersItemsCreateService {

    private final OrdersRepository ordersRepository;

    private final OrdersItemRepository ordersItemRepository;

    private final ServiceExecutor serviceExecutor;

    @Transactional
    @Override
    public List<OrdersItemResponse> execute(OrdersItemsCreateRequest request) {

        Orders order = getOrder(request);

        List<OrdersItem> ordersItems = saveOrdersItem(request,order);

        return buildResponse(ordersItems);
    }

    private List<OrdersItemResponse> buildResponse(List<OrdersItem> ordersItems) {
        return ordersItems.stream()
                .map(ordersItem -> {
                    OrdersItemResponse ordersItemResponse = new OrdersItemResponse();
                    BeanUtils.copyProperties(ordersItem,ordersItemResponse);
                    ordersItemResponse.setId(ordersItem.getId());
                    ordersItemResponse.setOrdersId(ordersItem.getOrders().getId());
                    ordersItemResponse.setEntity(ordersItem);
                    return ordersItemResponse;
                }).toList();
    }

    /*private Orders getOrder(OrdersItemsCreateRequest request) {

        return ordersRepository.findById(request.getOrdersId())
                .orElseThrow(() -> new OrdersNotFoundException(OrdersCodeResult.NOT_FOUND.getMessage(),
                        Arrays.asList(
                                ErrorResponse.builder()
                                        .code(OrdersCodeResult.NOT_FOUND.getHttpStatusCode())
                                        .field("ordersId")
                                        .message(OrdersCodeResult.NOT_FOUND.getMessage())
                                        .build()
                        )
                ));
    }*/

    private List<OrdersItem> saveOrdersItem(OrdersItemsCreateRequest request, Orders order) {

        List<OrdersItem> ordersItems = new ArrayList<>();

        for (OrdersItemCreateRequest item : request.getOrdersItems()) {
            OrdersItem ordersItem = new OrdersItem();
            BeanUtils.copyProperties(item,ordersItem);
            ordersItem.setCreateDate(new Date());
            ordersItem.setOrders(order);
            ordersItemRepository.save(ordersItem);
            ordersItems.add(ordersItem);
            if (item.getQty() != null && item.getQty() == 3) {
                throwException();
            }
        }

        return ordersItems;

    }

    private void throwException() {

        throw new OrdersNotFoundException(OrdersCodeResult.NOT_FOUND.getMessage(),
                Arrays.asList(
                        ErrorResponse.builder()
                                .code(OrdersCodeResult.NOT_FOUND.getHttpStatusCode())
                                .message(OrdersCodeResult.NOT_FOUND.getMessage())
                                .build()
                )
        );
    }

   private Orders getOrder(OrdersItemsCreateRequest request) {

        return serviceExecutor.execute(OrdersFindByIdService.class, UniqueRequest.<Long>builder()
                        .value(request.getOrdersId())
                .build()).getEntity();
    }

}

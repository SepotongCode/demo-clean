package com.demo.clean.service.query.contract;

import com.demo.clean.base.command.Command;
import com.demo.clean.base.dto.request.UniqueRequest;
import com.demo.clean.dto.response.OrdersResponse;

public interface OrdersFindByIdService extends Command<UniqueRequest<Long>, OrdersResponse> {
}

package com.demo.clean.service.query.service;

import com.demo.clean.base.dto.request.UniqueRequest;
import com.demo.clean.base.dto.response.ErrorResponse;
import com.demo.clean.dto.response.OrdersResponse;
import com.demo.clean.entity.Orders;
import com.demo.clean.enums.OrdersCodeResult;
import com.demo.clean.exceptions.thrower.OrdersNotFoundException;
import com.demo.clean.repository.OrdersRepository;
import com.demo.clean.service.query.contract.OrdersFindByIdService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class OrdersFindByIdServiceImpl implements OrdersFindByIdService {

    private final OrdersRepository ordersRepository;

    @Override
    public OrdersResponse execute(UniqueRequest<Long> request) {

        Orders order = getOrder(request);

        return buildResponse(order);

    }

    private OrdersResponse buildResponse(Orders order) {
        OrdersResponse ordersResponse = new OrdersResponse();
        BeanUtils.copyProperties(order,ordersResponse);
        ordersResponse.setEntity(order);
        return ordersResponse;
    }

    private Orders getOrder(UniqueRequest<Long> request) {
        return ordersRepository.findById(request.getValue())
                .orElseThrow(() -> new OrdersNotFoundException(OrdersCodeResult.NOT_FOUND.getMessage(),
                        Arrays.asList(
                                ErrorResponse.builder()
                                        .code(OrdersCodeResult.NOT_FOUND.getHttpStatusCode())
                                        .field("id")
                                        .message(OrdersCodeResult.NOT_FOUND.getMessage())
                                        .build()
                        )
                ));
    }

}

package com.demo.clean;

import com.demo.clean.base.controller.advice.controller.HttpExceptionAdvice;
import com.demo.clean.base.controller.advice.controller.KafkaExceptionAdvice;
import com.demo.clean.base.dto.response.ErrorResponse;
import com.demo.clean.base.exceptions.thrower.DataNotFoundException;
import com.demo.clean.enums.OrdersCodeResult;
import com.demo.clean.exceptions.thrower.OrdersExistException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URI;
import java.util.Arrays;

@Slf4j
@SpringBootTest
class DemoCleanApplicationTests {

    @Autowired
    private HttpExceptionAdvice httpExceptionAdvice;

    @Autowired
    private KafkaExceptionAdvice kafkaExceptionAdvice;

    @Test
    void exceptionCatcherTest() {

        try {
            throw new OrdersExistException("error", Arrays.asList(
                    ErrorResponse.builder()
                            .code(OrdersCodeResult.EXISTS.getHttpStatusCode())
                            .message(OrdersCodeResult.EXISTS.getMessage())
                            .build()
            ));
        } catch (Exception ex) {
            var httpException = httpExceptionAdvice.handleHttpException(ex);
            log.info("httpException: {}", httpException);
            log.info("==============================================");
            var kafkaException = kafkaExceptionAdvice.handleHttpException(ex);
            log.info("kafkaException: {}", kafkaException);
        }

    }

    @Test
    void coba() throws Exception {

        String [] splitValues = "camelValue".split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
        StringBuilder joinValue = new StringBuilder();
        int index = 0;
        for (String value : splitValues) {
            if (index > 0) {
                joinValue.append(".");
            }
            joinValue.append(value.toLowerCase());
            index++;
        }
        System.out.printf(joinValue.toString());
    }


}
